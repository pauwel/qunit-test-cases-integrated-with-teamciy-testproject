QUnit.test("Initialize service and sign in", function (assert) {
    //const done = assert.async();
    console.log("herp");
    var service = new Vidyano.Service("https://vidyano.azurewebsites.net");
    console.log("derp", service);
    // const service = new Vidyano.Service("http://localhost:18385/");
    service.initialize().then(function () {
        assert.ok(service.isSignedIn, "Signed in to service");
        assert.ok(service.isUsingDefaultCredentials, "Service is using default credentials");
        assert.equal(service.userName, "demo", "Service is signed in with user 'demo'");
        assert.equal(Vidyano.CultureInfo.currentCulture.name, "en-US", "Culture set to en-US");
    }).catch(function (e) { return assert.equal(e, ""); }).then();
});
