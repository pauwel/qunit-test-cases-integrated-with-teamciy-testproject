﻿module("Calcuator tests");

test("Optelling", function () {
    var som = optellen(2, 3);
    equal(5, som, "som van 2 + 3 is 5");
});

test("optelling van negatieve waardes", function() {
    var negasom = optellen(-2, -3);
    equal(-5, negasom, "som van -2 en -3 is -5");
});

test("Verminderen", function () {
    var verm = verminderen(5, 1);
    equal(4, verm, "5 verminderen met 1 is 4");
});

test("Vermenigvuldigen", function() {
    var maal = vermenigvuldigen(5, 5);
    equal(25, maal, "5 maal 5 is 25");
});